import admin from "firebase-admin";
import {cert} from './cred/cert';
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "firebase-adminsdk-evfwb@wr-node-promo3.iam.gserviceaccount.com",
});

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import {
    deleteHotel,
    getAllHostels, getHotelByRoomNumbers,
    getHotelWidthRooms,
    patchHotel,
    postNewHotel,
    putHotel
} from "./services/hostels.service";
import {
    deleteRoom,
    getRoomById,
    getRoomState,
    patchRoom,
    postNewRoom,
    putRoom,
    setRoomState
} from "./services/rooms.service";
import {RoomState} from "./models/room.model";

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/api/hostels/', async (req, res) => {
    try {
        const hostels = await getAllHostels();
        return res.send(hostels);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.post('/api/hostels/', async (req, res) => {
    try {
        const newHotel = req.body;
        const hotelToPost = await postNewHotel(newHotel);
        return res.send(hotelToPost);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const hotelToDelete = await deleteHotel(id);
        return res.send(hotelToDelete);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.put('/api/hostels/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const newHotel = req.body;
        const hotelToPut = await putHotel(id, newHotel);
        return res.send(hotelToPut)
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.patch('/api/hostels/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const newHotel = req.body;
        const hotelToPatch = await patchHotel(id, newHotel);
        return res.send(hotelToPatch);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});

app.get('/api/hostels/:id', async (req, res) => {
   try {
       const id: string = req.params.id;
       const hotel = await getHotelWidthRooms(id);
       return res.send(hotel);
   } catch (e) {
       return res.status(500).send(e.message);
   }
});
app.get('/api/rooms/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const room = await getRoomById(id);
        return res.send(room);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.post('/api/hostels/:id', async (req, res) => {
    try {
        const id: string = req.params.id;
        const newRoom = req.body;
        const roomToPost = await postNewRoom(id, newRoom);
        return res.send(roomToPost);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.delete('/api/hostels/:hotelId/rooms/:roomId', async (req, res) => {
   try {
       const hotelId: string = req.params.hotelId;
       const roomId: string = req.params.roomId;
       const roomToDelete = await deleteRoom(hotelId, roomId);
       return res.send(roomToDelete);
   } catch (e) {
       return res.status(500).send(e.message);
   }
});
app.put('/api/rooms/:id', async (req, res) => {
   try {
       const id: string = req.params.id;
       const newRoom = req.body;
       const roomToPut = await putRoom(id, newRoom);
       return res.send(roomToPut);
   } catch (e) {
       return res.status(500).send(e.message);
   }
});
app.patch('/api/rooms/:id', async (req, res) => {
   try {
       const id: string = req.params.id;
       const newRoom = req.body;
       const roomToPatch = await patchRoom(id, newRoom);
       return res.send(roomToPatch);
   } catch (e) {
       return res.status(500).send(e.message);
   }
});

// QUERY
app.get('/api/rooms', async (req, res) => {
    try {
        const roomNumbers = parseInt(req.query.roomNumbers);
        const result = await getHotelByRoomNumbers(roomNumbers);
        return res.send({result});
    } catch (e) {
        return res.status(500).send(e.message);
    }
});

// ENUMS
app.patch('/api/rooms/:id/state', async (req, res) => {
    try {
        const id: string = req.params.id;
        const state: RoomState = req.body.roomState;
        const result: string = await setRoomState(id, state);
        return res.send(result);
    } catch (e) {
        return res.status(500).send(e.message);
    }
});
app.get('/api/rooms/:id/state', async (req, res) => {
    try {
        const id: string = req.params.id;
        const state: RoomState = await getRoomState(id);
        return res.send({state});
    } catch (e) {
        return res.status(500).send(e.message);
    }
});

app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
