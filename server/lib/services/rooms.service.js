"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const room_model_1 = require("../models/room.model");
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const db = firebase_admin_1.default.firestore();
const refRooms = db.collection('rooms');
const refHotels = db.collection('hostels');
async function getRoomById(id) {
    const refRoom = await refRooms.doc(id);
    const snapRoom = await refRoom.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists');
    }
    else {
        return Object.assign(Object.assign({}, snapRoom.data()), { id: id });
    }
}
exports.getRoomById = getRoomById;
async function getAllRoomsByUids(uids) {
    const promisesToResolve = uids
        .map((uid) => getRoomById(uid));
    return Promise.all(promisesToResolve);
}
exports.getAllRoomsByUids = getAllRoomsByUids;
async function postNewRoom(id, newRoom) {
    const refHotel = await refHotels.doc(id);
    const snapHotel = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('hotel does not exists');
    }
    const addInRooms = await refRooms.add(newRoom);
    const addInTeam = await refHotels
        .doc(id)
        .collection('rooms')
        .doc(addInRooms.id);
    addInTeam.set({ ref: addInRooms, uid: addInRooms.id });
    return newRoom;
}
exports.postNewRoom = postNewRoom;
async function deleteRoom(hotelId, roomId) {
    const refHotel = refHotels.doc(hotelId);
    const snapHotel = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('hotel does not exists');
    }
    await refHotels
        .doc(hotelId)
        .collection('rooms')
        .doc(roomId).delete();
    await refRooms.doc(roomId).delete();
    return 'room deleted';
}
exports.deleteRoom = deleteRoom;
async function putRoom(id, newRoom) {
    const roomRef = refRooms.doc(id);
    const snapRoom = await roomRef.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists');
    }
    await roomRef.set(newRoom);
    return newRoom;
}
exports.putRoom = putRoom;
async function patchRoom(id, newRoom) {
    const roomRef = refRooms.doc(id);
    const snapRoom = await roomRef.get();
    if (!snapRoom.exists) {
        throw new Error('player does not exists');
    }
    await roomRef.update(newRoom);
    return newRoom;
}
exports.patchRoom = patchRoom;
// ENUMS
function checkRoomsState(state) {
    return Object.keys(room_model_1.RoomState).some((roomState) => roomState === state);
}
async function setRoomState(id, state) {
    if (!id) {
        throw new Error('id is missing');
    }
    if (!checkRoomsState(state)) {
        throw new Error('state is invalid');
    }
    const refRoom = await refRooms.doc(id);
    const snapRoom = await refRoom.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists');
    }
    await refRoom.update({ roomState: state });
    return 'ok';
}
exports.setRoomState = setRoomState;
async function getRoomState(id) {
    const room = await getRoomById(id);
    return room.roomState;
}
exports.getRoomState = getRoomState;
//# sourceMappingURL=rooms.service.js.map