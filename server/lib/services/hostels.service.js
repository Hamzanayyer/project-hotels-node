"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const rooms_service_1 = require("./rooms.service");
const db = firebase_admin_1.default.firestore();
const refHotels = db.collection('hostels');
async function getAllHostels() {
    const refHotel = await refHotels.get();
    const hostels = [];
    refHotel.forEach(hotel => hostels.push(Object.assign(Object.assign({}, hotel.data()), { id: hotel.id })));
    return hostels;
}
exports.getAllHostels = getAllHostels;
async function postNewHotel(newHotel) {
    const ref = await refHotels.add(newHotel);
    return Object.assign(Object.assign({}, newHotel), { id: ref.id });
}
exports.postNewHotel = postNewHotel;
async function deleteHotel(id) {
    const hotelRef = await refHotels.doc(id);
    await hotelRef.delete();
    return { response: `${id} -> 'delete ok` };
}
exports.deleteHotel = deleteHotel;
async function putHotel(id, newHotel) {
    const hotelRef = await refHotels.doc(id);
    await hotelRef.set(newHotel);
    return newHotel;
}
exports.putHotel = putHotel;
async function patchHotel(hotelId, newHotel) {
    const hotelRef = await refHotels.doc(hotelId);
    await hotelRef.update(newHotel);
    return newHotel;
}
exports.patchHotel = patchHotel;
async function getHotelById(id) {
    const refHotel = await refHotels.doc(id);
    const snapHotel = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('incorrect id');
    }
    else {
        return snapHotel.data();
    }
}
exports.getHotelById = getHotelById;
async function getHotelWidthRooms(id) {
    const hotelToFind = await getHotelById(id);
    const rooms = refHotels
        .doc(id)
        .collection('rooms');
    const snapRooms = await rooms.get();
    const hotelWidthRooms = [];
    snapRooms.forEach(room => hotelWidthRooms.push(room.data().uid));
    hotelToFind.roomsData = await rooms_service_1.getAllRoomsByUids(hotelWidthRooms);
    return hotelToFind;
}
exports.getHotelWidthRooms = getHotelWidthRooms;
// QUERY
async function getHotelByRoomNumbers(roomNumbers) {
    if (!roomNumbers) {
        throw new Error('roomNumbers required');
    }
    const data = await refHotels.where('roomNumbers', '==', roomNumbers).get();
    const result = [];
    data.forEach(doc => result.push(doc.data()));
    return result;
}
exports.getHotelByRoomNumbers = getHotelByRoomNumbers;
//# sourceMappingURL=hostels.service.js.map