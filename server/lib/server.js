"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const cert_1 = require("./cred/cert");
firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert(cert_1.cert),
    databaseURL: "firebase-adminsdk-evfwb@wr-node-promo3.iam.gserviceaccount.com",
});
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const hostels_service_1 = require("./services/hostels.service");
const rooms_service_1 = require("./services/rooms.service");
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
app.get('/api/hostels/', async (req, res) => {
    try {
        const hostels = await hostels_service_1.getAllHostels();
        return res.send(hostels);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.post('/api/hostels/', async (req, res) => {
    try {
        const newHotel = req.body;
        const hotelToPost = await hostels_service_1.postNewHotel(newHotel);
        return res.send(hotelToPost);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotelToDelete = await hostels_service_1.deleteHotel(id);
        return res.send(hotelToDelete);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.put('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newHotel = req.body;
        const hotelToPut = await hostels_service_1.putHotel(id, newHotel);
        return res.send(hotelToPut);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.patch('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newHotel = req.body;
        const hotelToPatch = await hostels_service_1.patchHotel(id, newHotel);
        return res.send(hotelToPatch);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.get('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await hostels_service_1.getHotelWidthRooms(id);
        return res.send(hotel);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.get('/api/rooms/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const room = await rooms_service_1.getRoomById(id);
        return res.send(room);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.post('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newRoom = req.body;
        const roomToPost = await rooms_service_1.postNewRoom(id, newRoom);
        return res.send(roomToPost);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.delete('/api/hostels/:hotelId/rooms/:roomId', async (req, res) => {
    try {
        const hotelId = req.params.hotelId;
        const roomId = req.params.roomId;
        const roomToDelete = await rooms_service_1.deleteRoom(hotelId, roomId);
        return res.send(roomToDelete);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.put('/api/rooms/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newRoom = req.body;
        const roomToPut = await rooms_service_1.putRoom(id, newRoom);
        return res.send(roomToPut);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.patch('/api/rooms/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const newRoom = req.body;
        const roomToPatch = await rooms_service_1.patchRoom(id, newRoom);
        return res.send(roomToPatch);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
// QUERY
app.get('/api/rooms', async (req, res) => {
    try {
        const roomNumbers = parseInt(req.query.roomNumbers);
        const result = await hostels_service_1.getHotelByRoomNumbers(roomNumbers);
        return res.send({ result });
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
// ENUMS
app.patch('/api/rooms/:id/state', async (req, res) => {
    try {
        const id = req.params.id;
        const state = req.body.roomState;
        const result = await rooms_service_1.setRoomState(id, state);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.get('/api/rooms/:id/state', async (req, res) => {
    try {
        const id = req.params.id;
        const state = await rooms_service_1.getRoomState(id);
        return res.send({ state });
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map