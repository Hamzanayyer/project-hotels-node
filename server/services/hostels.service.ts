import {HotelModel} from "../models/hotel.model";
import admin from "firebase-admin";

import QuerySnapshot = FirebaseFirestore.QuerySnapshot;
import CollectionReference = FirebaseFirestore.CollectionReference;
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import {getAllRoomsByUids} from "./rooms.service";

const db = admin.firestore();
const refHotels = db.collection('hostels');

export async function getAllHostels(): Promise<HotelModel>  {
    const refHotel: QuerySnapshot = await refHotels.get();
    const hostels: any = [];
    refHotel.forEach(hotel => hostels.push({
        ...hotel.data(),
        id: hotel.id,
    } as HotelModel));
    return hostels;
}
export async function postNewHotel(newHotel: HotelModel): Promise<HotelModel> {
    const ref: DocumentReference = await refHotels.add(newHotel);
    return {...newHotel, id: ref.id}
}
export async function deleteHotel(id: string): Promise<any> {
    const hotelRef: DocumentReference = await refHotels.doc(id);
    await hotelRef.delete();
    return {response: `${id} -> 'delete ok`};
}
export async function putHotel(id: string, newHotel: HotelModel): Promise<HotelModel> {
    const hotelRef: DocumentReference = await refHotels.doc(id);
    await hotelRef.set(newHotel);
    return newHotel;
}
export async function patchHotel(hotelId: string, newHotel: HotelModel): Promise<HotelModel> {
    const hotelRef: DocumentReference = await refHotels.doc(hotelId);
    await hotelRef.update(newHotel);
    return newHotel;
}

export async function getHotelById(id: string): Promise<HotelModel> {
    const refHotel: DocumentReference = await refHotels.doc(id);
    const snapHotel: DocumentSnapshot = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('incorrect id');
    } else {
        return snapHotel.data() as HotelModel;
    }
}
export async function getHotelWidthRooms(id: string): Promise<HotelModel> {
    const hotelToFind = await getHotelById(id);
    const rooms: CollectionReference = refHotels
        .doc(id)
        .collection('rooms');
    const snapRooms: QuerySnapshot = await rooms.get();
    const hotelWidthRooms: string[] = [];
    snapRooms.forEach(room => hotelWidthRooms.push(room.data().uid));
    hotelToFind.roomsData = await getAllRoomsByUids(hotelWidthRooms);
    return hotelToFind;
}

// QUERY
export async function getHotelByRoomNumbers(roomNumbers: number): Promise<HotelModel[]> {
    if (!roomNumbers) {
        throw new Error('roomNumbers required');
    }
    const data = await refHotels.where('roomNumbers', '==', roomNumbers).get();
    const result: HotelModel[] = [];
    data.forEach(doc => result.push(doc.data() as HotelModel));
    return result;
}
