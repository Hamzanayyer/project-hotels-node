import {RoomModel, RoomState} from "../models/room.model";
import admin from "firebase-admin";
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;

const db = admin.firestore();
const refRooms = db.collection('rooms');
const refHotels = db.collection('hostels');

export async function getRoomById(id: string): Promise<RoomModel> {
    const refRoom: DocumentReference = await refRooms.doc(id);
    const snapRoom: DocumentSnapshot = await refRoom.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists');
    } else {
        return {...snapRoom.data(), id: id} as RoomModel
    }
}
export async function getAllRoomsByUids(uids: string[]) {
    const promisesToResolve = uids
        .map((uid) => getRoomById(uid));
    return Promise.all(promisesToResolve);
}

export async function postNewRoom(id: string, newRoom: RoomModel): Promise<RoomModel> {
    const refHotel: DocumentReference = await refHotels.doc(id);
    const snapHotel: DocumentSnapshot = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('hotel does not exists');
    }
    const addInRooms: DocumentReference = await refRooms.add(newRoom);
    const addInTeam = await refHotels
        .doc(id)
        .collection('rooms')
        .doc(addInRooms.id);
    addInTeam.set({ref: addInRooms, uid: addInRooms.id});
    return newRoom;
}
export async function deleteRoom(hotelId: string, roomId: string): Promise<string> {
    const refHotel: DocumentReference = refHotels.doc(hotelId);
    const snapHotel: DocumentSnapshot = await refHotel.get();
    if (!snapHotel.exists) {
        throw new Error('hotel does not exists');
    }
    await refHotels
        .doc(hotelId)
        .collection('rooms')
        .doc(roomId).delete();
    await refRooms.doc(roomId).delete();
    return 'room deleted';
}
export async function putRoom(id: string, newRoom: RoomModel): Promise<RoomModel> {
    const roomRef: DocumentReference = refRooms.doc(id);
    const snapRoom: DocumentSnapshot = await roomRef.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists')
    }
    await roomRef.set(newRoom);
    return newRoom;
}
export async function patchRoom(id: string, newRoom: RoomModel): Promise<RoomModel> {
    const roomRef: DocumentReference = refRooms.doc(id);
    const snapRoom: DocumentSnapshot = await roomRef.get();
    if (!snapRoom.exists) {
        throw new Error('player does not exists')
    }
    await roomRef.update(newRoom);
    return newRoom;
}

// ENUMS
function checkRoomsState(state: any) {
    return Object.keys(RoomState).some((roomState) => roomState === state);
}
export async function setRoomState(id: string, state: RoomState): Promise<string> {
    if (!id) {
        throw new Error('id is missing');
    }
    if (!checkRoomsState(state)) {
        throw new Error('state is invalid');
    }
    const refRoom: DocumentReference = await refRooms.doc(id);
    const snapRoom: DocumentSnapshot = await refRoom.get();
    if (!snapRoom.exists) {
        throw new Error('room does not exists');
    }
    await refRoom.update({roomState: state});
    return 'ok';
}
export async function getRoomState(id: string): Promise<RoomState> {
    const room: RoomModel = await getRoomById(id);
    return room.roomState;
}
